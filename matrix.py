import os

'''
This method reads children and parents directories from Cornell kinship db 
URL http://chenlab.ece.cornell.edu/projects/KinshipVerification/
and maps them into a matrix where each row is a parent-child pair.
It returns the matrix.


ChildrenDir=full path to cornell's children directory
parentsDir=full path to cornell's parents directory
'''
def createMatrix(childrenDir, parentsDir):
	matrix=[]
	children=sorted(os.listdir(childrenDir))
	parents=sorted(os.listdir(parentsDir))
	
	for index, parent in enumerate(parents):
		cc=[os.path.join(parentsDir, parent), os.path.join(childrenDir, children[index])]
		matrix.append(cc)
	return matrix    
	
def main():
    matrix=createMatrix("Children","Parents")
    print(matrix)

if __name__ == "__main__":
    main()

